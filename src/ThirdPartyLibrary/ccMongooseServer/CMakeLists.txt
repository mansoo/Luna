#
CMAKE_MINIMUM_REQUIRED (VERSION 2.8)

#
PROJECT(ccMongooseServer)

# Configuration
#

add_definitions(
	-DMG_DISABLE_CGI
	-DMG_ENABLE_HTTP_STREAMING_MULTIPART
)

include_directories( ./ ../../Library ../../FossLibrary ../../ThirdPartyLibrary)

file(GLOB SRC_FILES 
	src/*.cpp
	src/mongoose/*.c
)

add_library (ccMongooseServer
	${SRC_FILES}
)
