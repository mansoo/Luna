# Luna
Very tiny SW Platform for Web API using C++ and mongoose web server.

## How to build libraries
### Linux & OS X
```bash
mkdir build  
cd build
cmake ..
make
```

## How to build a test application to test Web Server and Websocket
### Linux
```bash
cd ./Luna/build
./bin/WebServerTest
```
